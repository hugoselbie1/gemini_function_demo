import streamlit as st
from chat_functions import company_insights_tool, get_stock_price_from_api, get_company_news_from_api, get_company_overview_from_api, get_news_with_sentiment_from_api
import variables as var 
import time
import vertexai
from vertexai.preview.generative_models import (
    Content,
    GenerationConfig,
    FunctionDeclaration,
    GenerativeModel,
    Part,
    Tool,
)
from utils.functions_helper import get_text

vertexai.init(project=var.var['project_id'], location=var.var['region'])
import vertexai
from vertexai.generative_models import (
    FunctionDeclaration,
    GenerationConfig,
    GenerativeModel,
    Part,
    Tool,
)
gemini_model = GenerativeModel(
    "gemini-1.0-pro-001",
    generation_config=GenerationConfig(temperature=0),
    tools=[company_insights_tool],
)

chat = gemini_model.start_chat()
function_handler = {
    "get_stock_price": get_stock_price_from_api,
    "get_company_overview": get_company_overview_from_api,
    "get_company_news": get_company_news_from_api,
    "get_news_with_sentiment": get_news_with_sentiment_from_api,
}
# Streamed response emulator
def send_chat_message(prompt):
    print(prompt, "\n")
    prompt += """
    Give a concise, high-level summary. Only use information that you learn from 
    the API responses. 
    """

    # Send a chat message to the Gemini API
    response = chat.send_message(prompt)

    # Handle cases with multiple chained function calls
    function_calling_in_process = True
    while function_calling_in_process:
        # Extract the function call response
        function_call = response.candidates[0].content.parts[0].function_call

        # Check for a function call or a natural language response
        if function_call.name in function_handler.keys():
            # Extract the function call
            function_call = response.candidates[0].content.parts[0].function_call

            # Extract the function call name
            function_name = function_call.name
            print(function_name, "\n")

            # Extract the function call parameters
            params = {key: value for key, value in function_call.args.items()}
            print(params, "\n")

            # Invoke a function that calls an external API
            function_api_response = function_handler[function_name](params, var.var['api_key'])[
                :20000
            ]  # Stay within the input token limit
            print(function_api_response[:500], "...", "\n")

            # Send the API response back to Gemini, which will generate a natural language summary or another function call
            response = chat.send_message(
                Part.from_function_response(
                    name=function_name,
                    response={"content": function_api_response},
                ),
            )
            for word in response.text:
                yield word.replace('$', '\$')
                time.sleep(0.05)
        else:
            function_calling_in_process = False

gemini_model = GenerativeModel(
    "gemini-1.0-pro-001",
    generation_config=GenerationConfig(temperature=0),
    tools=[company_insights_tool],
)
st.title("Function Demo")

# Initialize chat history
if "messages" not in st.session_state:
    st.session_state.messages = []

# Display chat messages from history on app rerun
for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

# Accept user input
if prompt := st.chat_input("What is up?"):
    # Add user message to chat history
    st.session_state.messages.append({"role": "user", "content": prompt})
    # Display user message in chat message container
    with st.chat_message("user"):
        st.markdown(prompt)

    # Display assistant response in chat message container
    with st.chat_message("assistant"):
        response = st.write_stream(send_chat_message(prompt))
    # Add assistant response to chat history
    st.session_state.messages.append({"role": "assistant", "content": response})