import json
from vertexai.generative_models import (
    FunctionDeclaration,
    GenerativeModel,
    GenerationConfig,
    GenerationResponse,
    Tool,
)
from proto.marshal.collections import repeated
from proto.marshal.collections import maps

def get_text(response: GenerationResponse):
    """Returns the Text from the Generation Response object."""
    part = response.candidates[0].content.parts[0]
    try:
        text = part.text
    except:
        text = None

    return text