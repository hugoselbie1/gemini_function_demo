# gemini_function_demo
This is a basic function calling demo based on an example from the GCP demo repo found here using [streamlit](https://streamlit.io/) as a frontend [link](https://github.com/GoogleCloudPlatform/generative-ai/blob/main/gemini/function-calling/use_case_company_news_and_insights.ipynb) 

To try this set up your virtual env, install the requirements file and then execute the streamlit command `streamlit run app.py`. 